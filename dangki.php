<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dangki.css">
</head>
<body>
    <?php
        $gender=array('0' => 'Nam', '1' => 'Nữ');
        $pkhoa=array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
        $nameErr = $gtinhErr = $khoaErr = $ngsinhErr = "";
        $name = $gtinh = $khoa = $ngsinh = $dchi = "";
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["name"])) {
              $nameErr = "Hãy nhập tên";
            } else {
              $name = input($_POST["name"]);
            }
            
            if (empty($_POST["gtinh"])) {
              $gtinhErr = "Hãy chọn giới tính";
            } else {
              $gtinh = input($_POST["gtinh"]);
            }
              
            if (empty($_POST["khoa"])) {
              $khoaErr = "Hãy chọn phân khoa";
            } else {
              $khoa = input($_POST["khoa"]);
            }
          
            if (empty($_POST["date"])) {
              $ngsinhErr = "Hãy nhập ngày sinh";
            } else if (!validateDate($_POST["date"])){
                $ngsinhErr = "Hãy nhập ngày sinh đúng định dạng";
            } else {
                $ngsinh = $_POST["date"];
            }
          
            if (empty($_POST["dchi"])) {
              $dchi = "";
            } else {
              $dchi = input($_POST["dchi"]);
            }
        }
        function input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        function validateDate($date){
            $dates  = explode('/', $date);
            if (count($dates) == 3) {
                return checkdate($dates[1], $dates[0], $dates[2]);
            }
            return false;
        }
    ?>
    <form method="post" action="<?php 
         echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <p><?php
            if ($nameErr != "") echo $nameErr."<br>";
            if ($gtinhErr != "") echo $gtinhErr."<br>";
            if ($khoaErr != "") echo $khoaErr."<br>";
            if ($ngsinhErr != "") echo $ngsinhErr."<br>";
        ?></p>
        <div class="name">
            <label class="lab">Họ và tên <span class="error">* </span></label>
            <input class="inp" name="name" type="text"/></div>
        </div>
        <div class="name">
            <label class="lab">Giới tính <span class="error">* </span></label>
            <?php
            for ($i = 0; $i < count($gender); $i++) {
                echo "<input type=\"radio\" class=\"ip radio\" name=\"gtinh\" value=\"gtinh\">
                    <label for=\"{$i}\" class=\"gtinh\">{$gender[$i]}</label>\n";
            };
            ?>
        </div>
        <div class="name">
            <label class="lab">Phân Khoa <span class="error">* </span></label>
                <select name="khoa" id="khoa" class="ip khoa">
                    <option value=""></option>';
                    <?php
                    foreach ($pkhoa as $key => $value) {
                        echo "\t<option value=\"{$key}\">{$value}</option>\n";
                    };
                    ?>
                </select>
        </div>
        <div class="name">
            <label class="lab" for="date">Ngày sinh <span class="error">* </span></label>
            <input id="date" type="text" class="ip date" name="date" placeholder="dd/mm/yyyy"/>
        </div>
        <div class="name">
            <label class="lab">Địa chỉ</label>
            <input class="inp" type="text" /></div>
        </div>
        <button class="dki">Đăng kí</button>
    </form>
    
    <script type="text/javascript">
          $('#date').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'dd/mm/yyyy',
          });
    </script>
</body>
</html>